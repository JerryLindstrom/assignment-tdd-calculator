package com.company;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

   @Test
    public void getFirstValue() {
        //Arrange
        int firstNumberTest = 1;
        int expected = 1;
        Calculator calculator = new Calculator();

        //Act
       int actual = Calculator.getFirstNumber(firstNumberTest);

       //Assert
        assertEquals(expected, actual);
    }

    @Test
    public void getSecondValue() {
       int secondNumberTest = 1;
       int expected = 1;
       Calculator calculator = new Calculator();

       int actual = Calculator.getSecondNumber(secondNumberTest);

       assertEquals(expected, actual);
    }

    @Test
    public void getFirstAndSecondValue_NumbersAddedTogether_ReturnResultOfAddition() {
       int firstNumberTest = 1;
       int secondNumberTest = 1;
       int expected = 2;
       Calculator calculator = new Calculator();

       int actual = Calculator.addition(firstNumberTest, secondNumberTest);

       assertEquals(expected, actual);
    }

    @Test
    public void getFirstAndSecondValue_NumbersSubtracted_ReturnResultOfSubtraction() {
       int firstNumberTest = 1;
       int secondNumberTest = 1;
       int expected = 0;
       Calculator calculator = new Calculator();

       int actual = Calculator.subtraction(firstNumberTest, secondNumberTest);

       assertEquals(expected, actual);
    }

    @Test
    public void getFirstAndSecondValue_NumbersMultiplied_ReturnResultOfMultiplication() {
        int firstNumberTest = 2;
        int secondNumberTest = 2;
        int expected = 4;
        Calculator calculator = new Calculator();

        int actual = Calculator.multiplication(firstNumberTest, secondNumberTest);

        assertEquals(expected, actual);
    }

    @Test
    public void getFirstAndSecondValue_NumbersDivided_ReturnResultOfDivision() throws divideException {
       int firstNumberTest = 2;
       int secondNumberTest = 1;
       int expected = 2;
       Calculator calculator = new Calculator();

       int actual = Calculator.division(firstNumberTest, secondNumberTest);

       assertEquals(expected, actual);

    }

    @Test
    public void TestInputDivideByZero_ReturnException() {
       int firstTestNumber = 1;
       int lowerTestNumber = 0;
       Calculator calculator = new Calculator();

       assertThrows(divideException.class, () -> Calculator.division(firstTestNumber, lowerTestNumber));
    }

}